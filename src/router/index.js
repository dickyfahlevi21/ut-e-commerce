import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import NProgress from "nprogress";
import "nprogress/nprogress.css";

const routes = [{
    path: "/",
    component: () =>
      import( /* webpackChunkName: "Dashboard" */ "@/views/Main.view"),
    meta: {
      requiresAuth: true
    },
    children: [{
        path: "",
        name: "Dashboard",
        component: () =>
          import( /* webpackChunkName: "Dashboard" */ "@/views/Dashboard.view"),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: "detail",
        name: "DetailProduct",
        component: () =>
          import(
            /* webpackChunkName: "Product-detail" */
            "@/views/ProductDetail.view"
          ),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: "/products",
    name: "Products",
    component: () =>
      import( /* webpackChunkName: "Products" */ "@/views/Products.view"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/product-detail/:id",
    name: "ProductDetail",
    component: () =>
      import(
        /* webpackChunkName: "Product-detail" */
        "@/views/ProductDetail.view"
      ),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/sign-in",
    name: "SignIn",
    component: () =>
      import( /* webpackChunkName: "sign-in" */ "@/views/auth/SignIn.view"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/sign-up",
    name: "SignUp",
    component: () =>
      import( /* webpackChunkName: "sign-up" */ "@/views/auth/SignUp.view"),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/cart-detail",
    name: "CartDetail",
    component: () =>
      import( /* webpackChunkName: "cart-detail" */ "@/views/CartDetail.view")
  },
  {
    path: "/cart-detail/product-checkout",
    name: "ProductCheckout",
    component: () =>
      import(
        /* webpackChunkName: "cart-detail" */
        "@/views/ProductCheckout.view"
      )
  },
  // {
  //   path: "/cart-detail",
  //   redirect: {
  //     name: "CartDetail",
  //   },
  //   children: [{
  //       path: "",
  //       name: "CartDetail",
  //       component: () =>
  //         import( /* webpackChunkName: "cart-detail" */ "@/views/CartDetail.view")
  //     },
  //     {
  //       path: "product-checkout",
  //       name: "ProductCheckout",
  //       component: () =>
  //         import( /* webpackChunkName: "product-checkout" */ "@/views/ProductCheckout.view")
  //     },
  //   ]
  // },
  {
    // children : profile, address, store, history, voucher
    path: "/setting",
    redirect: {
      name: "Profiles"
    },
    component: () =>
      import( /* webpackChunkName: "settings" */ "@/views/Settings.view"),
    children: [{
        path: "profile",
        name: "Profiles",
        component: () =>
          import(
            /* webpackChunkName: "profiles" */
            "@/components/Settings/ProfileContent"
          )
      },
      {
        path: "address",
        name: "Addresses",
        component: () =>
          import(
            /* webpackChunkName: "addresses" */
            "@/components/Settings/AddressContent"
          )
      },
      {
        path: "store",
        name: "Stores",
        component: () =>
          import(
            /* webpackChunkName: "stores" */
            "@/components/Settings/StoreContent"
          ),
        meta: {
          blockUser: true
        }
      },
      {
        path: "history",
        name: "Histories",
        component: () =>
          import(
            /* webpackChunkName: "histories" */
            "@/components/Settings/HistoryContent"
          )
      },
      {
        path: "voucher",
        name: "Vouchers",
        component: () =>
          import(
            /* webpackChunkName: "vouchers" */
            "@/components/Settings/VoucherContent"
          ),
        meta: {
          blockUser: true
        }
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  {
    from,
    next;
  }
  if (to.matched.some(record => record.meta.blockUser)) {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user.type == "user") {
      Vue.$toast.warning("You don't have access to this page!");
      next({
        name: "Profiles"
      });
    } else {
      next();
    }
  }
  if (to.matched.some(record => !record.meta.requiresAuth)) {
    if (!localStorage.getItem("token")) {
      next({
        name: "SignIn"
      });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

router.beforeResolve((to, from, next) => {
  {
    from;
  }
  if (to.name) {
    NProgress.start();
  }
  next();
});

router.afterEach((to, from) => {
  {
    to,
    from;
  }
  NProgress.done();
});

export default router;