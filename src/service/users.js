import Api from "./api";
import { headerauth } from "@/helper/header";

export default {
  getId(id) {
    return Api.get("user/" + id, headerauth);
  },
  update(payload) {
    return Api.put("user", { data: payload.data }, headerauth);
  }
};
