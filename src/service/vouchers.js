import Api from "./api";
import { headerauth } from "@/helper/header";

export default {
  all() {
    return Api.get("disc?limit=1000&page=1", headerauth);
  },
  add(payload) {
    return Api.post("disc", { data: payload }, headerauth);
  },
  del(id) {
    return Api.delete("disc/" + id, headerauth);
  },
  delAll() {
    return Api.delete("disc/del", headerauth);
  }
};
