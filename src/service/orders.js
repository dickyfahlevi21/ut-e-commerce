import Api from "./api";
import { headerauth } from "@/helper/header";

export default {
  all() {
    return Api.get("order?limit=1000&page=1", headerauth);
  }
};
