import axios from "axios";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import Vue from "vue";

const token = localStorage.getItem("token");
axios.defaults.headers.common = {
  Authorization: `bearer ${token}`
};

const instance = axios.create({
  baseURL: `https://be-ut.herokuapp.com/api/v1`,
  headers: {
    "Content-Type": "application/json"
  }
});

instance.interceptors.request.use(
  config => {
    NProgress.start();
    return config;
  },
  error => Promise.reject(error)
);
instance.interceptors.response.use(
  function(response) {
    NProgress.done();
    return response;
  },
  function(error) {
    NProgress.done();
    Vue.$toast.error(error.message);
    return Promise.reject(error);
  }
);

export default instance;
