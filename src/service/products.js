import Api from "./api";
import { headerauth, headerform } from "@/helper/header";

export default {
  all() {
    return Api.get("product?limit=1000&page=1", headerauth);
  },
  search(q) {
    return Api.get("search?query=" + q, headerauth);
  },
  getId(id) {
    return Api.get("product/" + id, headerauth);
  },
  add(payload) {
    return Api.post("product", payload, headerform);
  },
  update(payload) {
    return Api.put("product/" + payload.id, payload.data, headerform);
  },
  del(id) {
    return Api.delete("product/" + id, headerauth);
  },
  delAll() {
    return Api.delete("product/del", headerauth);
  }
};
