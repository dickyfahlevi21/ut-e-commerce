import Api from "./api";
import { headerauth } from "@/helper/header.js";

export default {
  getcity() {
    return Api.get("ongkos");
  },
  getAll() {
    return Api.get("address?limit=1000&page=1", headerauth);
  },
  getOne(id) {
    return Api.get("address/" + id, headerauth);
  },
  addOne(payload) {
    return Api.post("address", payload, headerauth);
  },
  putOne(payload) {
    return Api.put("address/" + payload.id, payload.data, headerauth);
  },
  delOne(id) {
    return Api.delete("address/" + id, headerauth);
  }
};
