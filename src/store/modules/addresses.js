import AddressApi from "@/service/addresses";
export default {
  namespaced: true,
  state: () => ({
    dataCities: [],
    dataAddress: []
  }),
  mutations: {
    setCities(state, payload) {
      state.dataCities = payload;
    },
    setAddress(state, payload) {
      state.dataAddress = payload.data;
    }
  },
  actions: {
    async getCities({ commit }) {
      await AddressApi.getcity()
        .then(res => {
          commit("setCities", res.data);
        })
        .catch(err => alert(err.response.data.message));
    },
    async getAddress({ commit }) {
      await AddressApi.getAll()
        .then(res => {
          commit("setAddress", res.data);
        })
        .catch(err => alert(err.response));
    }
  }
};
