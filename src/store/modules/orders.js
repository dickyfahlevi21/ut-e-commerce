import OrderApi from "@/service/orders";
import router from "@/router/index";
import Vue from "vue";

export default {
  namespaced: true,
  state: () => ({
    dataOrder: []
  }),
  mutations: {
    getOrdersList(state, payload) {
      state.dataOrder = payload.data;
    }
  },
  actions: {
    async getOrders({ commit }) {
      await OrderApi.all()
        .then(res => {
          commit("getOrdersList", res.data);
        })
        .catch(err => {
          Vue.$toast.warning(err.response.data.message);
          router.go({
            name: "Histories"
          });
        });
    }
  }
};
