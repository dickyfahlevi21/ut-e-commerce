import UserApi from "@/service/users";
import router from "@/router/index";
import Vue from "vue";

export default {
  namespaced: true,
  state: () => ({
    detail: []
  }),
  mutations: {
    setDetail(state, payload) {
      state.detail = payload;
    }
  },
  actions: {
    async getId({ commit }, payload) {
      try {
        await UserApi.get(payload)
          .then(res => {
            const {
              data: { data }
            } = res;
            commit("setDetail", data);
          })
          .catch(err => Vue.$toast.warning(err.response.data.message));
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
      }
    },
    async updateProfiles(_, payload) {
      try {
        await UserApi.update(payload)
          .then(res => {
            localStorage.setItem("user", JSON.stringify(res.data.data));
            Vue.$toast.open(res.data.message);
            router.go({
              name: "Profiles"
            });
          })
          .catch(err => Vue.$toast.warning(err.response.data.message));
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
      }
    }
  }
};
