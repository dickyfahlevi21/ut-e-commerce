import VoucherApi from "@/service/vouchers";
import router from "@/router/index";
import Vue from "vue";

export default {
  namespaced: true,
  state: () => ({
    dataVoucher: []
  }),
  mutations: {
    getVouchersList(state, payload) {
      state.dataVoucher = payload.data;
    }
  },
  actions: {
    async getVouchers({ commit }) {
      await VoucherApi.all()
        .then(res => {
          commit("getVouchersList", res.data);
        })
        .catch(err => Vue.$toast.warning(err.response.data.message));
    },
    async addVouchers(_, payload) {
      try {
        await VoucherApi.add(payload)
          .then(res => {
            Vue.$toast.open(res.data.message);
            router.go({
              name: "Vouchers"
            });
          })
          .catch(err => {
            Vue.$toast.warning(err.response.data.message);
            router.go({
              name: "Vouchers"
            });
          });
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
        router.go({
          name: "Vouchers"
        });
      }
    },
    async delVoucher(_, payload) {
      try {
        await VoucherApi.del(payload)
          .then(res => {
            Vue.$toast.open(res.data.message);
            router.go({
              name: "Vouchers"
            });
          })
          .catch(err => {
            Vue.$toast.warning(err.response.data.message);
            router.go({
              name: "Vouchers"
            });
          });
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
        router.go({
          name: "Vouchers"
        });
      }
    },
    async delAllVouchers() {
      try {
        await VoucherApi.delAll()
          .then(res => {
            Vue.$toast.open(res.data.message);
            router.go({
              name: "Vouchers"
            });
          })
          .catch(err => {
            Vue.$toast.warning(err.response.data.message);
            router.go({
              name: "Vouchers"
            });
          });
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
        router.go({
          name: "Vouchers"
        });
      }
    }
  }
};
