import AuthApi from "@/service/auth";
import router from "@/router/index";
import Vue from "vue";

export default {
  namespaced: true,
  state: () => ({
    token: "",
    user: {
      id: "",
      firstName: "",
      lastName: "",
      email: "",
      phoneNumber: "",
      username: "",
      type: ""
    },
    isError: false,
    errorMessage: ""
  }),
  mutations: {
    saveLogin(state, payload) {
      state.token = payload.token;
      state.user = {
        id: payload.id,
        firstName: payload.firstName,
        lastName: payload.lastName,
        email: payload.email,
        phoneNumber: payload.phoneNumber,
        username: payload.username,
        type: payload.type
      };
    }
  },
  actions: {
    async reqLogin({ commit }, payload) {
      await AuthApi.login(payload)
        .then(res => {
          const {
            data: { data }
          } = res;
          commit("saveLogin", data);
          // console.log({
          //   tokenmu: data.token
          // });
          localStorage.setItem("token", data.token);
          localStorage.setItem(
            "user",
            JSON.stringify({
              id: data.id,
              firstName: data.firstName,
              lastName: data.lastName,
              email: data.email,
              phoneNumber: data.phoneNumber,
              username: data.username,
              type: data.type
            })
          );
          Vue.$toast.open(res.data.message);
          router.push({
            path: "/"
          });
        })
        .catch(err => Vue.$toast.warning(err.response.data.message));
    },
    async register(_, payload) {
      console.log(payload);
      await AuthApi.reg(payload)
        .then(res => {
          const {
            data: { data }
          } = res;
          Vue.$toast.open(res.data.message);
          Vue.$toast.open(data);
          router.push({
            name: "SignIn"
          });
        })
        .catch(err => Vue.$toast.warning(err.response.data.message));
    },
    logout() {
      let keysToRemove = ["vuex", "user", "token", "randid"];
      for (let key of keysToRemove) {
        localStorage.removeItem(key);
      }
      router.go({
        name: "Dashboard"
      });
    }
  }
};
