import ProductApi from "@/service/products";
import router from "@/router/index";
import Vue from "vue";

export default {
  namespaced: true,
  state: () => ({
    dataProduct: [],
    detail: [],
    search: []
  }),
  mutations: {
    getProductsList(state, payload) {
      state.dataProduct = payload.data;
    },
    setDetail(state, payload) {
      state.detail = payload;
    },
    setSearch(state, payload) {
      state.search = payload;
    }
  },
  actions: {
    async getProducts({ commit }) {
      await ProductApi.all()
        .then(res => {
          commit("getProductsList", res.data);
        })
        .catch(err => {
          Vue.$toast.warning(err.response.data.message);
          router.go({
            name: "Stores"
          });
        });
    },
    async addProducts(_, payload) {
      try {
        await ProductApi.add(payload)
          .then(res => {
            Vue.$toast.open(res.data.message);
            router.go({
              name: "Stores"
            });
          })
          .catch(err => Vue.$toast.warning(err.response.data.message));
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
      }
    },
    async delProduct(_, payload) {
      try {
        await ProductApi.del(payload)
          .then(res => {
            Vue.$toast.open(res.data.message);
            router.go({
              name: "Stores"
            });
          })
          .catch(err => Vue.$toast.warning(err.response.data.message));
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
      }
    },
    async delAllProducts() {
      try {
        await ProductApi.delAll()
          .then(res => {
            Vue.$toast.open(res.data.message);
            router.go({
              name: "Stores"
            });
          })
          .catch(err => Vue.$toast.warning(err.response.data.message));
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
      }
    },
    async getById({ commit }, payload) {
      try {
        await ProductApi.getId(payload)
          .then(res => {
            const {
              data: { data }
            } = res;
            commit("setDetail", data);
          })
          .catch(err => Vue.$toast.warning(err.response.data.message));
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
        router.push({
          name: "Products"
        });
      }
    },
    async updateProducts(_, payload) {
      try {
        await ProductApi.update(payload)
          .then(res => {
            Vue.$toast.open(res.data.message);
          })
          .catch(err => {
            Vue.$toast.warning(err.response.data.message);
          });
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
      }
    },
    async searchProducts({ commit }, payload) {
      try {
        await ProductApi.search(payload)
          .then(res => {
            commit("setSearch", res.data);
            router.go({
              name: "Products"
            });
          })
          .catch(err => {
            Vue.$toast.warning(err.response.data.message);
          });
      } catch (err) {
        Vue.$toast.warning(err.response.data.message);
        router.push({
          name: "Products"
        });
      }
    }
  }
};
